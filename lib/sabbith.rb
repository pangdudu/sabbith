require 'pry'
require 'dotenv'
Dotenv.load

require 'sabbith/version'
require 'sabbith/game'
require 'sabbith/api'
require 'sabbith/stock'
require 'sabbith/order'
require 'sabbith/sloan'

module Sabbith
  def self.start
    game = Game.new
    game.stop_level
    game.start_level
    stock = Stock.new(venue: game.config['venue'], stock_symbol: game.config['stock_symbol'])
    trader = Sloan.new(account: game.config['account'], stock: stock, qty: 100000)
    trader.trade
  end
end
