module Sabbith
  class Sloan
    LAST_PRICE_RESET_LIMIT = 25
    QTY_BASE = 2000
    MAX_OPEN_ORDERS = 10
    PRICE_RND = 100
    GATE_PRICE_DELTA = -75

    def initialize(account:, stock:, qty:)
      @account = account
      @stock = stock
      @qty = qty
      @acquired = 0
      @orders = []
      @prices = []
      @price_delta = 0
      @last_price = Float::INFINITY
      @last_price_reset = 0
      @tick = 0
    end

    def trade
      while @acquired < @qty
        update_last_price
        update_orders #if tick?(mod: 2)
        if open_orders <= MAX_OPEN_ORDERS
          price = new_price
          if @last_price > price || @price_delta < GATE_PRICE_DELTA
            puts "[#{Time.now}]Sloan#trade good price, new order" if @last_price > price
            puts "[#{Time.now}]Sloan#trade good delta, new order" if @price_delta < GATE_PRICE_DELTA
            new_order(price: price)
            reset_last_price(price: price)
          else
            puts "[#{Time.now}]Sloan#trade bad price, no new order"
          end
        end

        puts "[#{Time.now}]Sloan#trade acquired: '#{@acquired}'/'#{@qty}'"
      end
    rescue
      puts "[#{Time.now}]Sloan#trade something went wrong, restarting"
      trade
    end

    private

    def update_last_price
      @last_price_reset += 1
      if @last_price_reset >= LAST_PRICE_RESET_LIMIT
        @last_price = Float::INFINITY
        @price_delta = 0
        puts "[#{Time.now}]Sloan#update_last_price resetting last_price"
      else
        puts "[#{Time.now}]Sloan#update_last_price resetting last_price in #{LAST_PRICE_RESET_LIMIT - @last_price_reset}"
      end
    end

    def new_price
      price = best_price + Random.rand(PRICE_RND)
      puts "[#{Time.now}]Sloan#trade price: #{price}, last_price: #{@last_price}"
      price
    end

    def best_price
      @stock.best_bid
    rescue # TODO: do this more gracefully
      sleep 1
      @last_price
    end

    def reset_last_price(price:)
      update_prices(price: price)
      @last_price = price
      @last_price_reset = 0
    end


    def update_prices(price:)
      if @prices.length > 1
        @price_delta = price - @prices.last
        puts "[#{Time.now}] Sloan#update_prices: price_delta at: '#{@price_delta}'"
      end
      @prices << price
    end

    def new_order(price:)
      qty = Random.rand(QTY_BASE)
      order = Order.new(account: @account, stock: @stock)
      order.limit_order(qty: qty, price: price, direction: 'buy')
      @orders << order
      puts "[#{Time.now}]Sloan#new_order created new order with qty: '#{qty}', price: '#{price}'"
    end

    def open_orders
      @orders.map(&:status).
        reject {|s| s.nil? }.
        map {|s| s['open']}.
        select {|s| s.eql?(true)}.
        length
    end

    def update_orders
      puts "[#{Time.now}]Sloan#update_orders checking open orders '#{open_orders}'"
      @orders.each do |order|
        order.update_status if order.status.nil? || order.open?
      end
      @acquired = @orders.
        map {|order| order.status['totalFilled']}.
        inject(0) { |sum, x| sum + x }
    end

    def tick?(mod:)
      @tick += 1
      @tick % mod >= 1
    end
  end
end
