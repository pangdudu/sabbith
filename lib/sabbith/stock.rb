module Sabbith
  class Stock
    attr_reader :venue, :stock_symbol

    def initialize(venue:, stock_symbol:)
      @venue = venue
      @stock_symbol = stock_symbol
      @api = Api.new
    end

    def quote
      @api.quote(venue: venue, stock_symbol: stock_symbol)
    end

    def orderbook
      @api.orderbook(venue: venue, stock_symbol: stock_symbol)
    end

    # TODO: test
    def best_bid
      sorted_bids = orderbook['bids'].sort {|a,b| a['price'] <=> b['price']}
      sorted_bids.last['price']
    end

    private

    def asks
      orderbook['asks']
    end

    def bids
      orderbook['bids']
    end
  end
end
