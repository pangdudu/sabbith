require 'sabbith/game_api'

module Sabbith
  class Game
    attr_reader :config

    def initialize
      @api = GameApi.new
      @config = {}
    end

    def stop_level
      return unless File.exists?('config.json')
      load_config
      if @config['instance']
        @api.stop_level(instance: @config['instance'])
        puts "[#{Time.now}]Game#stop_level stopped instance: '#{@config['instance']}'"
      else
        puts "[#{Time.now}]Game#stop_level no previous instance found"
      end
    end

    def start_level
      response = @api.start_level
      @config = {
        'instance' => response['instanceId'],
        'account' => response['account'],
        'venue' => response['venues'].first,
        'stock_symbol' => response['tickers'].first
      }
      save_config
      puts "[#{Time.now}]Game#start_level started instance: '#{@config['instance']}'"
    end

    private

    def load_config
      @config = JSON.parse(File.read('config.json'))
    end

    def save_config
      File.open('config.json','w') do |f|
        f.write(config.to_json)
      end
    end
  end
end
