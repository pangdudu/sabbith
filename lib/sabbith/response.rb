module Sabbith
  class Response
    def initialize(response)
      @response = response
    end

    def parsed
      @response.parsed_response
    end

    def ok?
      @response.parsed_response['ok'].eql?(true)
    end
  end
end
