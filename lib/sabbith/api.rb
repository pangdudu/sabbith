require 'httparty'
require 'sabbith/response'

module Sabbith
  class Api
    include HTTParty

    def up?
      get('/heartbeat').ok?
    end

    def venue_up?(venue:)
      get("/venues/#{venue}/heartbeat").ok?
    end

    def stocks(venue:)
      get("/venues/#{venue}/stocks").parsed['symbols']
    end

    def quote(venue:, stock_symbol:)
      get("/venues/#{venue}/stocks/#{stock_symbol}/quote").parsed
    end

    def orderbook(venue:, stock_symbol:)
      get("/venues/#{venue}/stocks/#{stock_symbol}").parsed
    end

    def order_status(venue:, stock_symbol:, id:)
      get("/venues/#{venue}/stocks/#{stock_symbol}/orders/#{id}").parsed
    end

    def order(account:, venue:, stock_symbol:, direction:, order_type:, price:, qty:)
      order = {
        account: account,
        venue: venue,
        stock: stock_symbol,
        price: price,
        qty: qty,
        direction: direction,
        orderType: order_type
      }
      post("/venues/#{venue}/stocks/#{stock_symbol}/orders", order).parsed
    end

    private

    def get(url)
      response = Response.new(self.class.get(url, options))
      return response if response.ok?
      puts "[#{Time.now}]Api#get: failed to get '#{url}', retrying."
      get(url)
    end

    def post(url, body)
      response = self.class.post(url, options.merge({body: body.to_json}))
      Response.new(response)
    end

    def options
      {
        base_uri: 'https://api.stockfighter.io/ob/api',
        headers: headers
      }
    end

    def headers
      {
        "X-Starfighter-Authorization" => ENV['APIKEY']
      }
    end
  end
end
