require 'httparty'

module Sabbith
  class GameApi
    include HTTParty

    def start_level
      post('/levels/chock_a_block', {}).parsed
    end

    def stop_level(instance:)
      post("/instances/#{instance}/stop", {}).parsed
    end

    private

    def post(url, body)
      response = self.class.post(url, options.merge({body: body.to_json}))
      Response.new(response)
    end

    def options
      {
        base_uri: 'https://api.stockfighter.io/gm',
        headers: headers
      }
    end

    def headers
      {
        "X-Starfighter-Authorization" => ENV['APIKEY']
      }
    end
  end
end
