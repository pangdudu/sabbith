module Sabbith
  class Order
    attr_reader :response, :status

    def initialize(account:, stock:)
      @account = account
      @stock = stock
      @api = Api.new
    end

    def limit_order(qty:, price:, direction:)
      @response = @api.order(account: @account, venue: @stock.venue, stock_symbol: @stock.stock_symbol,
                            direction: direction, order_type: 'limit',
                            price: price, qty: qty)
    end

    def open?
      @status['open'].eql?(true)
    end

    def update_status
      @status = @api.order_status(venue: @stock.venue, stock_symbol: @stock.stock_symbol, id: id)
    end

    def id
      @response['id']
    end
  end
end
