require 'spec_helper'

module Sabbith
  describe Api do
    subject { Api.new }

    describe '#up?' do
      context 'the api is up' do
        it 'returns true', vcr: true do
          expect(subject.up?).to eql(true)
        end
      end
    end

    describe '#venue_up?' do
      context 'the venue exists' do
        it 'returns true', vcr: true do
          expect(subject.venue_up?(venue: 'TESTEX')).to eql(true)
        end
      end
    end

    describe '#stocks' do
      it 'gets the stocks', vcr: true do
        expect(subject.stocks(venue: 'TESTEX')).to include(
          {
            "name"=>"Foreign Owned Occluded Bridge Architecture Resources",
            "symbol"=>"FOOBAR"
          })
      end
    end

    describe '#quote', vcr: true do
      subject { Api.new.quote(venue: 'TESTEX', stock_symbol: 'FOOBAR') }

      %w(symbol venue bid bidSize askSize bidDepth askDepth last lastSize lastTrade quoteTime).each do |key|
        it { is_expected.to include(key) }
      end
    end

    describe '#order_status', vcr: true do
      let(:stock) { Stock.new(venue: 'TESTEX', stock_symbol: 'FOOBAR') }
      let(:order) { Order.new(account: 'EXB123456', stock: stock) }

      subject do
        order.limit_order(qty: 1, price: 1000, direction: 'buy')
        Api.new.order_status(venue: 'TESTEX', stock_symbol: 'FOOBAR', id: order.id)
      end

      %w(ok symbol venue direction originalQty qty price orderType id account ts fills totalFilled open).each do |key|
        it { is_expected.to include(key) }
      end
    end

    describe '#orderbook', vcr: true do
      subject { Api.new.orderbook(venue: 'TESTEX', stock_symbol: 'FOOBAR') }

      %w(asks bids venue symbol ts ok).each do |key|
        it { is_expected.to include(key) }
      end
    end

    describe '#order' do
      context 'limit order', vcr: true do
        let(:options) { {account: 'EXB123456', venue: 'TESTEX', stock_symbol: 'FOOBAR',
                         direction: 'buy', order_type: 'limit',
                         price: 1337, qty: 100} }
        subject { Api.new.order(options) }

        it "posted the order correctly" do
          expect(subject['ok']).to eql(true)
          expect(subject['symbol']).to eql(options[:stock_symbol])
          %i(venue direction price qty).each do |key|
            expect(subject[key.to_s]).to eql(options[key])
          end
        end
      end
    end
  end
end
