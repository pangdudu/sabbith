require 'spec_helper'

module Sabbith
  describe Order do
    let(:stock) { Stock.new(venue: 'TESTEX', stock_symbol: 'FOOBAR') }
    subject { Order.new(account: 'EXB123456', stock: stock) }

    describe '#limit_order' do
      it 'uses the order api call' do
        expect_any_instance_of(Api).to receive(:order).
          with(account: 'EXB123456', venue: 'TESTEX', stock_symbol: 'FOOBAR',
               direction: 'buy', order_type: 'limit',
               price: 1337, qty: 100)
          subject.limit_order(price: 1337, qty: 100, direction: 'buy')
      end

      it 'stores the response and the order.id', vcr: true do
        subject.limit_order(price: 1337, qty: 100, direction: 'buy')
        expect(subject.response).to_not be_nil
        expect(subject.id).to_not be_nil
      end
    end

    describe '#update_status' do
      it 'uses the order_status api call' do
        expect(subject).to receive(:id).and_return(1)
        expect_any_instance_of(Api).to receive(:order_status).
          with(venue: 'TESTEX', stock_symbol: 'FOOBAR', id: 1)
        subject.update_status
      end
    end
  end
end
