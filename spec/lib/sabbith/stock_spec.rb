require 'spec_helper'

module Sabbith
  describe Stock do
    subject { Stock.new(venue: 'TESTEX', stock_symbol: 'FOOBAR') }

    describe '#orderbook' do
      it 'uses the orderbook api call' do
        expect_any_instance_of(Api).to receive(:orderbook).
          with(venue: 'TESTEX', stock_symbol: 'FOOBAR')
        subject.orderbook
      end
    end

    describe '#quote' do
      it 'uses the quote api call' do
        expect_any_instance_of(Api).to receive(:quote).
          with(venue: 'TESTEX', stock_symbol: 'FOOBAR')
        subject.quote
      end
    end
  end
end
