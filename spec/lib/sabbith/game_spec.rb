require 'spec_helper'

module Sabbith
  describe Game do
    subject { Game.new }

    describe '#stop_level' do
      it 'loads the config' do
        expect(subject).to receive(:load_config)
        subject.stop_level
      end

      it 'uses the stop_level api call' do
        expect(JSON).to receive(:parse).and_return({'instance' => 123})
        expect_any_instance_of(GameApi).to receive(:stop_level).with(instance: 123)
        subject.stop_level
      end
    end

    describe '#start_level' do
      it 'uses the start_level api call' do
        expect_any_instance_of(GameApi).to receive(:start_level).and_return({
          'instanceId' => 123,
          'account' => 'TEST',
          'venues' => ['TEST'],
          'tickers' => ['TEST']
        })
        subject.start_level
      end
    end
  end
end
